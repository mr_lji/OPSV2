# -*- coding: utf-8 -*-
# @Author     ：米斯特李 Mr.lee
# @DateTime   ：2019-08-18  13:52 
# @FileName   ：custom_filter.py.py
# @Ide        ：PyCharm

from rbac.models import UserProfile, Role
from assets.models import ServerAssets, NetAssets, IDC, SpareParts, SpareOut, Tags
import django_filters


class UserFilter(django_filters.rest_framework.FilterSet):
    # 使用conjoined=True 精确搜索
    roles = django_filters.ModelMultipleChoiceFilter(field_name="roles", label='角色',
                                                     queryset=Role.objects.filter(), conjoined=True)
    type = django_filters.CharFilter(field_name='type', label='类型')

    class Meta:
        model = UserProfile
        fields = ['roles', 'type']


class ServerFilter(django_filters.rest_framework.FilterSet):
    # 使用conjoined=True 精确搜索
    type = django_filters.CharFilter(field_name='type', label='类型')
    tags = django_filters.ModelMultipleChoiceFilter(field_name="tags", label='标签',
                                                    queryset=Tags.objects.filter(), conjoined=True)
    idc = django_filters.ModelChoiceFilter(field_name='idc', queryset=IDC.objects.filter(), label='IDC', )
    os_type = django_filters.CharFilter(field_name='os_type', label='系统类型')
    ip = django_filters.CharFilter(field_name='ip', label='IP')
    security_domain = django_filters.CharFilter(field_name='security_domain', label='安全域')
    device_type = django_filters.CharFilter(field_name='device_type', label='设备类型')
    status = django_filters.CharFilter(field_name='status', label='资产状态')

    class Meta:
        model = ServerAssets
        fields = ['type', 'os_type', 'security_domain', 'device_type', 'status', 'ip', 'tags']


class NetworkFilter(django_filters.rest_framework.FilterSet):
    # 使用conjoined=True 精确搜索
    device_type = django_filters.CharFilter(field_name='device_type', label='设备类型')
    management_ip = django_filters.CharFilter(field_name='management_ip', label='管理IP')
    status = django_filters.CharFilter(field_name='status', label='资产状态')

    class Meta:
        model = NetAssets
        fields = ['device_type', 'status', 'management_ip']


class SparePartsFilter(django_filters.rest_framework.FilterSet):
    type = django_filters.CharFilter(field_name='type', label='备件类型')
    status = django_filters.BooleanFilter(field_name='status', label='备件状态')
    in_date = django_filters.DateFromToRangeFilter(field_name='in_date', label='date')

    class Meta:
        model = SpareParts
        fields = ['type', 'status', 'in_date']


class SpareOutFilter(django_filters.rest_framework.FilterSet):
    rep_date = django_filters.DateFromToRangeFilter(field_name='rep_date', label='date')

    class Meta:
        model = SpareOut
        fields = ['rep_date']


class TagsFilter(django_filters.rest_framework.FilterSet):
    id = django_filters.Filter(field_name='id', label='id')

    class Meta:
        model = Tags
        fields = ['id', ]
