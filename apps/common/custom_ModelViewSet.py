# -*- coding: utf-8 -*-
# @Author     ：米斯特李 Mr.lee
# @DateTime   ：2019-08-16  23:50 
# @FileName   ：custom_ModelViewSet.py
# @Ide        ：PyCharm
from rest_framework.viewsets import ModelViewSet
from rest_framework import status
from apps.common.custom_response import MyResponse
from rest_framework.response import Response


class MyModelViewSet(ModelViewSet):
    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        return Response({'code': 200, 'msg': 'success', 'errors': {}, 'data': response.data},
                        headers=response.status_code)

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        return Response(ReturnMsg(data=response.data).dict(), status=response.status_code)


class ReturnMsg:
    def __init__(self, code=200, msg='success', errors=None, data=None):
        self.code = code
        self.msg = msg
        self.errors = {} if errors is None else errors
        self.data = [] if data is None else data

    def dict(self):
        return {
            'code': self.code,
            'msg': self.msg,
            'errors': self.errors,
            'data': self.data
        }
