from django.urls import path, include
from rbac.views import user, menu, role, permission
from rest_framework import routers
from django.views.decorators.csrf import csrf_exempt

router = routers.SimpleRouter()
router.register(r'users', user.UserViewSet, base_name="users")
router.register(r'menus', menu.MenuViewSet, base_name="menus")
router.register(r'permissions', permission.PermissionViewSet, base_name="permissions")
router.register(r'roles', role.RoleViewSet, base_name="roles")
urlpatterns = [
    path(r'api/', include(router.urls)),
    path(r'api/auth/login', csrf_exempt(user.UserAuthView.as_view()), name='user_login'),
    path(r'api/auth/info', user.UserInfoView.as_view(), name='user_info'),
    path(r'api/upload-avatar', user.UploadAvatar.as_view(), name='upload_avatar'),
    path(r'api/auth/build/menus/', user.UserBuildMenuView.as_view(), name='build_menus'),
    path(r'api/menu/tree/', menu.MenuTreeView.as_view(), name='menus_tree'),
    path(r'api/permission/tree/', permission.PermissionTreeView.as_view(), name='permissions_tree'),
    path(r'api/user/lists/', user.UserListView.as_view(), name='user_list'),
]
