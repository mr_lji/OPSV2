# Register your models here.
from django.contrib import admin
from rbac.models import UserProfile, Role, Permission,Menu

# Register your models here.


admin.site.register(UserProfile)
admin.site.register(Role)
admin.site.register(Permission)
admin.site.register(Menu)
