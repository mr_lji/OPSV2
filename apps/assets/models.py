from django.db import models


class DateAbstract(models.Model):
    """
    时间公共继承模型
    """
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


class IDC(DateAbstract):
    name = models.CharField(verbose_name='机房名', max_length=100)
    address = models.CharField(verbose_name='地址', max_length=200, blank=True, null=True)
    mobile = models.CharField(verbose_name='联系电话', max_length=100, blank=True, null=True)
    floor = models.CharField(verbose_name='楼层', max_length=100, blank=True, default='一楼')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'IDC数据中心'
        verbose_name_plural = verbose_name


class AssetsAbstract(models.Model):
    """
    资产公共继承模型
    """
    tags = models.ManyToManyField("Tags", blank=True, verbose_name="资产标签")
    remarks = models.TextField(verbose_name='备注', blank=True)
    shelves_date = models.CharField(verbose_name='上架日期', blank=True, max_length=50)
    maintenance_date = models.CharField(verbose_name='维保日期', blank=True, max_length=50)
    sn = models.CharField(verbose_name='资产SN', max_length=50, blank=True)
    model = models.CharField(max_length=100, blank=True, verbose_name='资产型号')
    brand = models.CharField(verbose_name='资产品牌', max_length=100, blank=True)
    device_type = models.CharField(verbose_name='设备类型', blank=True, max_length=50)
    status = models.CharField(verbose_name='服务器状态',
                              max_length=50, default='使用中')
    idc = models.ForeignKey(IDC, verbose_name='IDC', on_delete=models.SET_NULL, blank=True, null=True)
    cabinet = models.CharField(verbose_name='所在机柜', max_length=100, blank=True)

    class Meta:
        abstract = True


class Tags(DateAbstract):
    name = models.CharField(verbose_name='标签名', max_length=30, unique=True, blank=True, null=True)
    desc = models.CharField(verbose_name='描述', max_length=100, null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = '标签表'
        verbose_name_plural = verbose_name


class Dict(models.Model):
    key = models.CharField(max_length=80, verbose_name='键')
    value = models.CharField(max_length=80, verbose_name='值')
    desc = models.CharField(max_length=255, blank=True, null=True, verbose_name='描述')
    pid = models.ForeignKey(
        'self', blank=True, null=True, on_delete=models.SET_NULL, related_name='child'
    )

    class Meta:
        verbose_name = '数据字典'
        verbose_name_plural = verbose_name


class ServerAssets(AssetsAbstract, DateAbstract):
    ip = models.GenericIPAddressField(verbose_name='服务器IP', max_length=120, null=True)
    management_ip = models.CharField(verbose_name='远程管理IP', max_length=120,
                                     blank=True)
    other_ip = models.CharField(verbose_name='次IP', max_length=180, blank=True)
    type = models.CharField(verbose_name='服务器类型', max_length=100, blank=True)
    security_domain = models.CharField(verbose_name='安全域', max_length=100)
    auth_type = models.CharField(verbose_name='登录类型', max_length=100, blank=True)
    app_env = models.CharField(verbose_name='应用环境', max_length=100, blank=True)
    os_type = models.CharField(verbose_name='操作系统类型', max_length=50, blank=True)
    os_version = models.CharField(verbose_name='操作系统版本', max_length=100, blank=True)
    app = models.CharField(verbose_name='应用', max_length=140, blank=True)
    app_desc = models.CharField(verbose_name='应用描述', max_length=200, blank=True)
    username = models.CharField(verbose_name='服务器用户名', max_length=100, blank=True)
    password = models.CharField(verbose_name='服务器密码', max_length=100, blank=True)
    port = models.CharField(verbose_name='服务器端口号', blank=True, max_length=30)

    class Meta:
        verbose_name = '服务器资产表'
        verbose_name_plural = '服务器资产表'


class NetAssets(DateAbstract, AssetsAbstract):
    hostname = models.CharField(max_length=50, verbose_name="主机名", blank=True)
    management_ip = models.GenericIPAddressField(verbose_name="管理IP", max_length=15, blank=True, null=True)
    ip1 = models.CharField(verbose_name='IP1', max_length=20, blank=True)
    ip2 = models.CharField(verbose_name='IP2', max_length=20, blank=True)
    app = models.CharField(max_length=140, blank=True, verbose_name='应用')
    port = models.CharField(verbose_name='设备端口号', blank=True, max_length=30)
    management_segment = models.CharField(verbose_name='管理网段', max_length=50, blank=True)
    os_version = models.CharField(verbose_name='固件版本', max_length=50, blank=True)
    auth_type = models.CharField(verbose_name='认证方式', max_length=50, blank=True)
    up_link_port = models.CharField(verbose_name='上联端口', max_length=100, blank=True)
    up_device = models.CharField(verbose_name='上联设备', max_length=100, blank=True)
    web_url = models.CharField(max_length=100, blank=True, verbose_name='访问URL')
    username = models.CharField(max_length=20, blank=True, verbose_name='用户名')
    password = models.CharField(max_length=50, blank=True, verbose_name='密码')

    def __unicode__(self):
        return self.hostname

    class Meta:
        verbose_name = '网络设备'
        verbose_name_plural = verbose_name


class SpareParts(DateAbstract):
    type = models.CharField(max_length=100, verbose_name='备件类型')
    size = models.CharField(max_length=80, verbose_name='规格', blank=True)
    brand = models.CharField(max_length=100, blank=True, verbose_name='备件品牌')
    model = models.CharField(max_length=300, blank=True, verbose_name='备件型号')
    pn = models.CharField(max_length=50, blank=True, verbose_name='备件PN号')
    sn = models.CharField(max_length=50, verbose_name='备件SN号', unique=True)
    status = models.BooleanField(max_length=30, verbose_name='备件状态', default=True)
    in_date = models.CharField(verbose_name='入库日期', blank=True, max_length=100)
    warehouse = models.CharField(max_length=100, blank=True, verbose_name='备件仓库')
    remarks = models.TextField(blank=True, verbose_name='备注')

    def __str__(self):
        return self.sn

    class Meta:
        verbose_name = '备件表'
        verbose_name_plural = verbose_name


class SpareOut(DateAbstract):
    SpareParts = models.OneToOneField(SpareParts, on_delete=models.CASCADE, null=True)
    find_date = models.CharField(verbose_name='故障发现日期', blank=True, max_length=100)
    rep_date = models.CharField(verbose_name='更换日期', blank=True, max_length=100)
    model = models.CharField(verbose_name='设备型号', max_length=100, blank=True)
    sn = models.CharField(blank=True, max_length=100, verbose_name='设备SN号')
    ip = models.GenericIPAddressField(null=True, blank=True, max_length=100, verbose_name='设备ip号')
    bad_pn = models.CharField(blank=True, verbose_name='取出坏件pn', max_length=100)
    bad_sn = models.CharField(blank=True, verbose_name='取出坏件sn', max_length=100)
    fixed = models.BooleanField(verbose_name='是否已修', default=True)
    remarks = models.TextField(blank=True, verbose_name='备注')

    def __str__(self):
        return self.ip

    class Meta:
        verbose_name = '备件更换表'
        verbose_name_plural = verbose_name


class MiddleWare(DateAbstract):
    name = models.CharField(verbose_name='名称', max_length=120, blank=True)
    database = models.CharField(verbose_name='数据库', max_length=120, blank=True)
    environment = models.CharField(verbose_name='环境', max_length=120, blank=True)
    port = models.CharField(verbose_name='节点端口', max_length=240, blank=True)
    servers = models.CharField(verbose_name='服务器', max_length=120, blank=True)
    f5_url = models.CharField(verbose_name='F5地址', max_length=120, blank=True)
    contact = models.CharField(verbose_name='联系人', max_length=120, blank=True)
    developers = models.CharField(verbose_name='开发商', max_length=120, blank=True)
    patch = models.CharField(verbose_name='补丁', max_length=120, blank=True)
    remarks = models.TextField(blank=True, verbose_name='备注')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = '中间件信息表'
        verbose_name_plural = verbose_name
