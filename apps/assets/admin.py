# Register your models here.
from django.contrib import admin
from assets.models import ServerAssets, IDC, Tags, SpareParts, SpareOut, Dict, NetAssets, MiddleWare
from assets.views.excel_import import SPResource, SOResource
from import_export.admin import ImportExportModelAdmin


# Register your models here.

class SPAdmin(ImportExportModelAdmin):
    resource_class = SPResource


class SOAdmin(ImportExportModelAdmin):
    resource_class = SOResource


admin.site.register(IDC)
admin.site.register(Tags)
admin.site.register(MiddleWare)
admin.site.register(SpareParts, SPAdmin)
admin.site.register(ServerAssets)
admin.site.register(SpareOut, SOAdmin)
admin.site.register(Dict)
admin.site.register(NetAssets)
