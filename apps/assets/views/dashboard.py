# @Author     ：SEED
# @DateTime   ：2019-08-25  11:48 
# @FileName   ：dashboard.py

from rest_framework.views import APIView
from assets.models import ServerAssets, SpareParts,NetAssets
from django.http import JsonResponse
from itertools import product
from django.db.models.aggregates import Count


class DashboardData(APIView):
    def get(self, request, *args, **kwargs):
        server_count = ServerAssets.objects.count()
        sp_count = SpareParts.objects.count()
        network_count = NetAssets.objects.count()
        data = {
            'server_count': server_count,
            'sp_count': sp_count,
            'network_count':network_count,
            'server_type_data':[]
        }
        server_type_count = ServerAssets.objects.values('type').annotate(count=Count('type'))
        for n in server_type_count:
            data['server_type_data'].append({'name': n['type'], 'value': n['count']})
        return JsonResponse({'code': 200, 'data': data})
