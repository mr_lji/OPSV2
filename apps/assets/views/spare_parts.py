# @Author     ：SEED
# @DateTime   ：2019-08-20  11:12 
# @FileName   ：spare_parts.py

from assets.models import SpareParts
from assets.serializers.spare_parts_serializer import SparePartsSerializer
from rest_framework import viewsets
from common.custom import CustomPagination, RbacPermission
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from common.custom_filter import SparePartsFilter
from rest_framework_jwt.authentication import JSONWebTokenAuthentication


class SparePartsViewSet(viewsets.ModelViewSet):
    """
    备件
    """
    perms_map = (
        {'*': 'admin'}, {'*': 'sp_all'}, {'get': 'sp_list'}, {'post': 'sp_create'},
        {'put': 'sp_edit'},
        {'delete': 'sp_delete'})
    queryset = SpareParts.objects.all()
    serializer_class = SparePartsSerializer
    pagination_class = CustomPagination
    filter_class = SparePartsFilter
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    ordering_fields = ('in_date',)
    ordering = ('-in_date',)
    search_fields = ('type', 'size', 'warehouse', 'sn', 'pn', 'brand', 'model')
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (RbacPermission,)
