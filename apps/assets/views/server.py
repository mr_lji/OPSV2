# @Author     ：SEED
# @DateTime   ：2019-08-20  11:11 
# @FileName   ：server.py
from assets.models import ServerAssets
from import_export import resources
from import_export.fields import Field
from assets.serializers.server_serializer import ServerSerializer, ServerSerializerDepth
from rest_framework import viewsets
from rest_framework.views import APIView
from tablib import Dataset
from common.custom_filter import ServerFilter
from common.custom import CustomPagination, RbacPermission
from django.http import JsonResponse
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, status
from common.custom import MyResponse
from rest_framework_jwt.authentication import JSONWebTokenAuthentication


class ServerViewSet(viewsets.ModelViewSet):
    """
    备件管理
    """
    perms_map = ({'*': 'admin'}, {'*': 'server_all'}, {'get': 'server_list'}, {'post': 'server_create'},
                 {'put': 'server_edit'},
                 {'delete': 'server_delete'})
    queryset = ServerAssets.objects.all()
    serializer_class = ServerSerializer
    pagination_class = CustomPagination
    filter_class = ServerFilter
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    search_fields = (
        'ip', 'app', 'app_desc', 'management_ip', 'other_ip', 'type',
        'device_type', 'model', 'brand', 'sn', 'cabinet',
        'security_domain', 'remarks')
    ordering_fields = ('id',)
    ordering = ('-id',)

    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (RbacPermission,)

    def get_serializer_class(self):
        # 根据请求类型动态变更serializer
        if self.action == 'list':
            return ServerSerializerDepth
        return ServerSerializer
