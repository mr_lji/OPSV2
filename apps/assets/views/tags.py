# @Author     ：SEED
# @DateTime   ：2019-08-20  11:12 
# @FileName   ：tags.py

from assets.models import Tags
from assets.serializers.tags_serializer import TagsSerializer
from rest_framework import viewsets
from common.custom_filter import TagsFilter
from common.custom import CustomPagination, RbacPermission
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from rest_framework_jwt.authentication import JSONWebTokenAuthentication


class TagsViewSet(viewsets.ModelViewSet):
    """
    备件出库
    """
    perms_map = (
        {'*': 'admin'}, {'*': 'tags_all'}, {'get': 'tags_list'}, {'post': 'tags_create'},
        {'put': 'tags_edit'},
        {'delete': 'tags_delete'})
    queryset = Tags.objects.all()
    serializer_class = TagsSerializer
    pagination_class = CustomPagination
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('name',)
    filter_class = TagsFilter
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (RbacPermission,)
