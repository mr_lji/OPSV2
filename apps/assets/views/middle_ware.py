# @Author     ：SEED
# @DateTime   ：2019-12-03  15:23 
# @FileName   ：middle_ware.py


from assets.models import MiddleWare
from assets.serializers.middleware_serializer import MiddleWareSerializer
from rest_framework import viewsets
from common.custom import CustomPagination, RbacPermission
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from rest_framework_jwt.authentication import JSONWebTokenAuthentication


class MiddleWareViewSet(viewsets.ModelViewSet):
    """
    中间件管理
    """
    perms_map = ({'*': 'admin'}, {'*': 'mid_all'}, {'get': 'mid_list'}, {'post': 'mid_create'}, {'put': 'mid_edit'},
                 {'delete': 'mid_delete'})
    queryset = MiddleWare.objects.all()
    serializer_class = MiddleWareSerializer
    pagination_class = CustomPagination
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('name', 'servers', 'port', 'f5_url', 'database', 'patch', 'developers', 'remarks')
    ordering_fields = ('id',)
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (RbacPermission,)
