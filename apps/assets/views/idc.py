# @Author     ：SEED
# @DateTime   ：2019-08-20  11:11 
# @FileName   ：idc.py

from assets.models import IDC
from assets.serializers.idc_serializer import IdcSerializer
from rest_framework import viewsets
from common.custom import CustomPagination, RbacPermission
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from rest_framework_jwt.authentication import JSONWebTokenAuthentication


class IdcViewSet(viewsets.ModelViewSet):
    """
    idc管理
    """
    perms_map = ({'*': 'admin'}, {'*': 'idc_all'}, {'get': 'idc_list'}, {'post': 'idc_create'}, {'put': 'idc_edit'},
                 {'delete': 'idc_delete'})
    queryset = IDC.objects.all()
    serializer_class = IdcSerializer
    pagination_class = CustomPagination
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('name', 'mobile', 'address')
    ordering_fields = ('id',)
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (RbacPermission,)
