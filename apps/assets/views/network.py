# @Author     ：SEED
# @DateTime   ：2019-08-25  12:46 
# @FileName   ：network.py

from assets.models import NetAssets
from assets.serializers.network_serializer import NetSerializer, NetSerializerDepth
from rest_framework import viewsets
from common.custom_filter import NetworkFilter
from common.custom import CustomPagination, RbacPermission
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, status
from rest_framework_jwt.authentication import JSONWebTokenAuthentication


class NetViewSet(viewsets.ModelViewSet):
    """
    网络设备
    """
    perms_map = ({'*': 'admin'}, {'*': 'network_all'}, {'get': 'network_list'}, {'post': 'network_create'},
                 {'put': 'network_edit'},
                 {'delete': 'network_delete'})
    queryset = NetAssets.objects.all()
    serializer_class = NetSerializer
    pagination_class = CustomPagination
    filter_class = NetworkFilter
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    search_fields = (
        'app', 'management_ip', 'device_type', 'model', 'brand', 'sn', 'cabinet', 'remarks')
    ordering_fields = ('id',)
    ordering = ('-id',)

    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (RbacPermission,)

    def get_serializer_class(self):
        # 根据请求类型动态变更serializer
        if self.action == 'list':
            return NetSerializerDepth
        return NetSerializer



