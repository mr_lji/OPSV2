# @Author     ：SEED
# @DateTime   ：2019-08-20  11:30 
# @FileName   ：urls.py

from django.urls import path, include
from .views import idc, spare_out, spare_parts, tags, server, dict, dashboard, network,middle_ware
from rest_framework import routers
from .views import excel_import

router = routers.SimpleRouter()
router.register('server', server.ServerViewSet, base_name="server")
router.register('network', network.NetViewSet, base_name="network")
router.register('middle-ware', middle_ware.MiddleWareViewSet, base_name="middle_ware")
router.register('tags', tags.TagsViewSet, base_name="tags")
router.register('dict', dict.DictViewSet, base_name="dict")
router.register('spare-out', spare_out.SpareOutViewSet, base_name="spare_out")
router.register('spare-parts', spare_parts.SparePartsViewSet, base_name="spare_parts")
router.register('idc', idc.IdcViewSet, base_name="idc")
urlpatterns = [
    path('api/', include(router.urls)),
    path('api/dict/tree', dict.DictTreeView.as_view(), name='dict_tree'),
    path('api/dicts', dict.GetDictByKey.as_view()),
    path('api/import', excel_import.Import.as_view(), name='import'),
    path('api/dashboard', dashboard.DashboardData.as_view(), name='dashboard'),
    path('api/make-report', spare_out.SoReport.as_view(), name='make-report')
]
