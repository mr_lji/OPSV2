# @Author     ：SEED
# @DateTime   ：2019-08-20  11:50 
# @FileName   ：tags_serializer.py


from rest_framework import serializers
from assets.models import Tags


class TagsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tags
        fields = '__all__'
