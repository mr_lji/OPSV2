# @Author     ：SEED
# @DateTime   ：2019-08-20  19:34 
# @FileName   ：dict_serializer.py
from rest_framework import serializers

from assets.models import Dict


class DictSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dict
        fields = '__all__'


class DictTreeSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    label = serializers.CharField(max_length=20, source='value')
    pid = serializers.PrimaryKeyRelatedField(read_only=True)
