# @Author     ：SEED
# @DateTime   ：2019-08-20  11:42 
# @FileName   ：spare_parts_serializer.py

from rest_framework import serializers
from assets.models import SpareParts


class SparePartsSerializer(serializers.ModelSerializer):
    class Meta:
        model = SpareParts
        fields = '__all__'
