# @Author     ：SEED
# @DateTime   ：2019-08-20  11:27 
# @FileName   ：idc_serializer.py
from rest_framework import serializers
from assets.models import MiddleWare


class MiddleWareSerializer(serializers.ModelSerializer):
    class Meta:
        model = MiddleWare
        fields = '__all__'
