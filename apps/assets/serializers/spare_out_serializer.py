# @Author     ：SEED
# @DateTime   ：2019-08-20  11:36 
# @FileName   ：spare_out_serializer.py

from rest_framework import serializers
from assets.models import SpareOut


class SpareOutSerializer(serializers.ModelSerializer):
    class Meta:
        model = SpareOut
        fields = '__all__'
        depth = 1


class UpdateOutSerializer(serializers.ModelSerializer):
    sr_sp_sn = serializers.CharField(source='SpareParts.sp_sn', read_only=True, allow_null=True)

    class Meta:
        model = SpareOut
        fields = '__all__'
