# @Author     ：SEED
# @DateTime   ：2019-08-20  11:52 
# @FileName   ：server_serializer.py

from rest_framework import serializers
from assets.models import ServerAssets


# 重写ChoiceField 显示字段
class DisplayChoiceField(serializers.ChoiceField):
    def to_representation(self, obj):
        """返回选项的值"""
        return self._choices[obj]


class ServerSerializer(serializers.ModelSerializer):
    class Meta:
        model = ServerAssets
        fields = '__all__'


class ServerSerializerDepth(serializers.ModelSerializer):
    # idc_name = serializers.CharField(source='server_idc.name', read_only=True)

    class Meta:
        model = ServerAssets
        fields = '__all__'
        depth = 1
