# @Author     ：SEED
# @DateTime   ：2019-08-20  11:27 
# @FileName   ：idc_serializer.py
from rest_framework import serializers
from assets.models import IDC


class IdcSerializer(serializers.ModelSerializer):
    class Meta:
        model = IDC
        fields = '__all__'
