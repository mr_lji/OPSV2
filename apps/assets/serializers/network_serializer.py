# @Author     ：SEED
# @DateTime   ：2019-08-25  12:45 
# @FileName   ：network_serializer.py


from rest_framework import serializers
from assets.models import NetAssets


class NetSerializer(serializers.ModelSerializer):
    class Meta:
        model = NetAssets
        fields = '__all__'


class NetSerializerDepth(serializers.ModelSerializer):
    class Meta:
        model = NetAssets
        fields = '__all__'
        depth = 1
