OPS平台v2
===========================
基于django+vue实现的一套前后端分离的运维系统

****
	
|Author|Mr.Li|
|---|---
|E-mail|hello.mr.lji@gmail.com

目前项目部分功能还未完成,建议仅作交流学习使用.



### 项目前端地址

[前端地址](https://gitee.com/mr_lji/OPS_QD "悬停显示")

前端：
Vue.js+ElementUi+Websocket

后端:
django+django rest framework+django channels+celery+farbic
### 更新日志

5/25：
新增菜单及权限初始化SQL文件，运行项目之前先导入ops2.sql文件

部署采用nginx+gunicorn+supervistor

### 部分截图预览：
登录
![Alt text](/static/image/login.png)
仪表盘
![Alt text](/static/image/dashboard.png)
导入
![Alt text](/static/image/import.png)
个人中心
![Alt text](/static/image/my.png)
资产管理
![Alt text](/static/image/server2.png)
WEBSSH
![Alt text](/static/image/webssh.png)