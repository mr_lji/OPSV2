/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50730
 Source Host           : localhost:3306
 Source Schema         : ops2

 Target Server Type    : MySQL
 Target Server Version : 50730
 File Encoding         : 65001

 Date: 28/05/2020 15:43:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for assets_dict
-- ----------------------------
DROP TABLE IF EXISTS `assets_dict`;
CREATE TABLE `assets_dict`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `value` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pid_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `assets_dict_pid_id_927637a5_fk_assets_dict_id`(`pid_id`) USING BTREE,
  CONSTRAINT `assets_dict_pid_id_927637a5_fk_assets_dict_id` FOREIGN KEY (`pid_id`) REFERENCES `assets_dict` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of assets_dict
-- ----------------------------
INSERT INTO `assets_dict` VALUES (1, 'SERVER_TYPES', 'SERVER_TYPES', '', NULL);
INSERT INTO `assets_dict` VALUES (2, 'SERVER_TYPE', 'SERVER_TYPE', '', NULL);
INSERT INTO `assets_dict` VALUES (3, 'WWW', 'WWWW', '', 2);
INSERT INTO `assets_dict` VALUES (4, 'DEVICE_TYPE', 'DEVICE_TYPE', '', NULL);
INSERT INTO `assets_dict` VALUES (5, '21312', '31231', '', 4);
INSERT INTO `assets_dict` VALUES (6, 'SYSTEM_TYPE', 'SYSTEM_TYPE', '', NULL);
INSERT INTO `assets_dict` VALUES (7, '1231231', '123213', '', 8);
INSERT INTO `assets_dict` VALUES (8, 'OS_TYPE', 'OS_TYPE', 'asdad', NULL);

-- ----------------------------
-- Table structure for assets_idc
-- ----------------------------
DROP TABLE IF EXISTS `assets_idc`;
CREATE TABLE `assets_idc`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` datetime(6) NOT NULL,
  `update_date` datetime(6) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `address` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `mobile` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `floor` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of assets_idc
-- ----------------------------

-- ----------------------------
-- Table structure for assets_middleware
-- ----------------------------
DROP TABLE IF EXISTS `assets_middleware`;
CREATE TABLE `assets_middleware`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` datetime(6) NOT NULL,
  `update_date` datetime(6) NOT NULL,
  `name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `database` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `environment` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `port` varchar(240) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `servers` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `f5_url` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `contact` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `developers` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `patch` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `remarks` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of assets_middleware
-- ----------------------------

-- ----------------------------
-- Table structure for assets_netassets
-- ----------------------------
DROP TABLE IF EXISTS `assets_netassets`;
CREATE TABLE `assets_netassets`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` datetime(6) NOT NULL,
  `update_date` datetime(6) NOT NULL,
  `remarks` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `shelves_date` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `maintenance_date` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sn` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `model` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `brand` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `device_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `cabinet` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `hostname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `management_ip` char(39) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ip1` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ip2` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `app` varchar(140) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `port` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `management_segment` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `os_version` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `auth_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `up_link_port` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `up_device` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `web_url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `idc_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `assets_netassets_idc_id_654a889c_fk_assets_idc_id`(`idc_id`) USING BTREE,
  CONSTRAINT `assets_netassets_idc_id_654a889c_fk_assets_idc_id` FOREIGN KEY (`idc_id`) REFERENCES `assets_idc` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of assets_netassets
-- ----------------------------

-- ----------------------------
-- Table structure for assets_netassets_tags
-- ----------------------------
DROP TABLE IF EXISTS `assets_netassets_tags`;
CREATE TABLE `assets_netassets_tags`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `netassets_id` int(11) NOT NULL,
  `tags_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `assets_netassets_tags_netassets_id_tags_id_47707825_uniq`(`netassets_id`, `tags_id`) USING BTREE,
  INDEX `assets_netassets_tags_tags_id_afafbdac_fk_assets_tags_id`(`tags_id`) USING BTREE,
  CONSTRAINT `assets_netassets_tag_netassets_id_f16b6274_fk_assets_ne` FOREIGN KEY (`netassets_id`) REFERENCES `assets_netassets` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `assets_netassets_tags_tags_id_afafbdac_fk_assets_tags_id` FOREIGN KEY (`tags_id`) REFERENCES `assets_tags` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of assets_netassets_tags
-- ----------------------------

-- ----------------------------
-- Table structure for assets_serverassets
-- ----------------------------
DROP TABLE IF EXISTS `assets_serverassets`;
CREATE TABLE `assets_serverassets`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` datetime(6) NOT NULL,
  `update_date` datetime(6) NOT NULL,
  `remarks` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `shelves_date` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `maintenance_date` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sn` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `model` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `brand` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `device_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `cabinet` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ip` char(39) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `management_ip` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `other_ip` varchar(180) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `security_domain` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `auth_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `app_env` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `os_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `os_version` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `app` varchar(140) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `app_desc` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `port` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `idc_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `assets_serverassets_idc_id_08f438e4_fk_assets_idc_id`(`idc_id`) USING BTREE,
  CONSTRAINT `assets_serverassets_idc_id_08f438e4_fk_assets_idc_id` FOREIGN KEY (`idc_id`) REFERENCES `assets_idc` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of assets_serverassets
-- ----------------------------
INSERT INTO `assets_serverassets` VALUES (1, '2020-05-28 07:40:01.050298', '2020-05-28 07:40:01.050298', '', '', '', '', '', '', '31231', '使用中', '', '127.0.0.1', '', '', 'WWWW', '内网', 'SSH', '', '123213', '', '', '', 'root', '', '22', NULL);

-- ----------------------------
-- Table structure for assets_serverassets_tags
-- ----------------------------
DROP TABLE IF EXISTS `assets_serverassets_tags`;
CREATE TABLE `assets_serverassets_tags`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serverassets_id` int(11) NOT NULL,
  `tags_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `assets_serverassets_tags_serverassets_id_tags_id_c21ff78e_uniq`(`serverassets_id`, `tags_id`) USING BTREE,
  INDEX `assets_serverassets_tags_tags_id_8666876c_fk_assets_tags_id`(`tags_id`) USING BTREE,
  CONSTRAINT `assets_serverassets__serverassets_id_e52a3f3c_fk_assets_se` FOREIGN KEY (`serverassets_id`) REFERENCES `assets_serverassets` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `assets_serverassets_tags_tags_id_8666876c_fk_assets_tags_id` FOREIGN KEY (`tags_id`) REFERENCES `assets_tags` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of assets_serverassets_tags
-- ----------------------------

-- ----------------------------
-- Table structure for assets_spareout
-- ----------------------------
DROP TABLE IF EXISTS `assets_spareout`;
CREATE TABLE `assets_spareout`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` datetime(6) NOT NULL,
  `update_date` datetime(6) NOT NULL,
  `find_date` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `rep_date` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `model` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sn` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ip` char(39) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bad_pn` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `bad_sn` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fixed` tinyint(1) NOT NULL,
  `remarks` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `SpareParts_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `SpareParts_id`(`SpareParts_id`) USING BTREE,
  CONSTRAINT `assets_spareout_SpareParts_id_76c5632f_fk_assets_spareparts_id` FOREIGN KEY (`SpareParts_id`) REFERENCES `assets_spareparts` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of assets_spareout
-- ----------------------------

-- ----------------------------
-- Table structure for assets_spareparts
-- ----------------------------
DROP TABLE IF EXISTS `assets_spareparts`;
CREATE TABLE `assets_spareparts`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` datetime(6) NOT NULL,
  `update_date` datetime(6) NOT NULL,
  `type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `size` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `brand` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `model` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `pn` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sn` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `in_date` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `warehouse` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `remarks` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `sn`(`sn`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of assets_spareparts
-- ----------------------------

-- ----------------------------
-- Table structure for assets_tags
-- ----------------------------
DROP TABLE IF EXISTS `assets_tags`;
CREATE TABLE `assets_tags`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` datetime(6) NOT NULL,
  `update_date` datetime(6) NOT NULL,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `desc` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of assets_tags
-- ----------------------------

-- ----------------------------
-- Table structure for auth_group
-- ----------------------------
DROP TABLE IF EXISTS `auth_group`;
CREATE TABLE `auth_group`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of auth_group
-- ----------------------------

-- ----------------------------
-- Table structure for auth_group_permissions
-- ----------------------------
DROP TABLE IF EXISTS `auth_group_permissions`;
CREATE TABLE `auth_group_permissions`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `auth_group_permissions_group_id_permission_id_0cd325b0_uniq`(`group_id`, `permission_id`) USING BTREE,
  INDEX `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm`(`permission_id`) USING BTREE,
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of auth_group_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for auth_permission
-- ----------------------------
DROP TABLE IF EXISTS `auth_permission`;
CREATE TABLE `auth_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `auth_permission_content_type_id_codename_01ab375a_uniq`(`content_type_id`, `codename`) USING BTREE,
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of auth_permission
-- ----------------------------
INSERT INTO `auth_permission` VALUES (1, 'Can add log entry', 1, 'add_logentry');
INSERT INTO `auth_permission` VALUES (2, 'Can change log entry', 1, 'change_logentry');
INSERT INTO `auth_permission` VALUES (3, 'Can delete log entry', 1, 'delete_logentry');
INSERT INTO `auth_permission` VALUES (4, 'Can view log entry', 1, 'view_logentry');
INSERT INTO `auth_permission` VALUES (5, 'Can add permission', 2, 'add_permission');
INSERT INTO `auth_permission` VALUES (6, 'Can change permission', 2, 'change_permission');
INSERT INTO `auth_permission` VALUES (7, 'Can delete permission', 2, 'delete_permission');
INSERT INTO `auth_permission` VALUES (8, 'Can view permission', 2, 'view_permission');
INSERT INTO `auth_permission` VALUES (9, 'Can add group', 3, 'add_group');
INSERT INTO `auth_permission` VALUES (10, 'Can change group', 3, 'change_group');
INSERT INTO `auth_permission` VALUES (11, 'Can delete group', 3, 'delete_group');
INSERT INTO `auth_permission` VALUES (12, 'Can view group', 3, 'view_group');
INSERT INTO `auth_permission` VALUES (13, 'Can add content type', 4, 'add_contenttype');
INSERT INTO `auth_permission` VALUES (14, 'Can change content type', 4, 'change_contenttype');
INSERT INTO `auth_permission` VALUES (15, 'Can delete content type', 4, 'delete_contenttype');
INSERT INTO `auth_permission` VALUES (16, 'Can view content type', 4, 'view_contenttype');
INSERT INTO `auth_permission` VALUES (17, 'Can add session', 5, 'add_session');
INSERT INTO `auth_permission` VALUES (18, 'Can change session', 5, 'change_session');
INSERT INTO `auth_permission` VALUES (19, 'Can delete session', 5, 'delete_session');
INSERT INTO `auth_permission` VALUES (20, 'Can view session', 5, 'view_session');
INSERT INTO `auth_permission` VALUES (21, 'Can add crontab', 6, 'add_crontabschedule');
INSERT INTO `auth_permission` VALUES (22, 'Can change crontab', 6, 'change_crontabschedule');
INSERT INTO `auth_permission` VALUES (23, 'Can delete crontab', 6, 'delete_crontabschedule');
INSERT INTO `auth_permission` VALUES (24, 'Can view crontab', 6, 'view_crontabschedule');
INSERT INTO `auth_permission` VALUES (25, 'Can add interval', 7, 'add_intervalschedule');
INSERT INTO `auth_permission` VALUES (26, 'Can change interval', 7, 'change_intervalschedule');
INSERT INTO `auth_permission` VALUES (27, 'Can delete interval', 7, 'delete_intervalschedule');
INSERT INTO `auth_permission` VALUES (28, 'Can view interval', 7, 'view_intervalschedule');
INSERT INTO `auth_permission` VALUES (29, 'Can add periodic task', 8, 'add_periodictask');
INSERT INTO `auth_permission` VALUES (30, 'Can change periodic task', 8, 'change_periodictask');
INSERT INTO `auth_permission` VALUES (31, 'Can delete periodic task', 8, 'delete_periodictask');
INSERT INTO `auth_permission` VALUES (32, 'Can view periodic task', 8, 'view_periodictask');
INSERT INTO `auth_permission` VALUES (33, 'Can add periodic tasks', 9, 'add_periodictasks');
INSERT INTO `auth_permission` VALUES (34, 'Can change periodic tasks', 9, 'change_periodictasks');
INSERT INTO `auth_permission` VALUES (35, 'Can delete periodic tasks', 9, 'delete_periodictasks');
INSERT INTO `auth_permission` VALUES (36, 'Can view periodic tasks', 9, 'view_periodictasks');
INSERT INTO `auth_permission` VALUES (37, 'Can add task state', 10, 'add_taskmeta');
INSERT INTO `auth_permission` VALUES (38, 'Can change task state', 10, 'change_taskmeta');
INSERT INTO `auth_permission` VALUES (39, 'Can delete task state', 10, 'delete_taskmeta');
INSERT INTO `auth_permission` VALUES (40, 'Can view task state', 10, 'view_taskmeta');
INSERT INTO `auth_permission` VALUES (41, 'Can add saved group result', 11, 'add_tasksetmeta');
INSERT INTO `auth_permission` VALUES (42, 'Can change saved group result', 11, 'change_tasksetmeta');
INSERT INTO `auth_permission` VALUES (43, 'Can delete saved group result', 11, 'delete_tasksetmeta');
INSERT INTO `auth_permission` VALUES (44, 'Can view saved group result', 11, 'view_tasksetmeta');
INSERT INTO `auth_permission` VALUES (45, 'Can add task', 12, 'add_taskstate');
INSERT INTO `auth_permission` VALUES (46, 'Can change task', 12, 'change_taskstate');
INSERT INTO `auth_permission` VALUES (47, 'Can delete task', 12, 'delete_taskstate');
INSERT INTO `auth_permission` VALUES (48, 'Can view task', 12, 'view_taskstate');
INSERT INTO `auth_permission` VALUES (49, 'Can add worker', 13, 'add_workerstate');
INSERT INTO `auth_permission` VALUES (50, 'Can change worker', 13, 'change_workerstate');
INSERT INTO `auth_permission` VALUES (51, 'Can delete worker', 13, 'delete_workerstate');
INSERT INTO `auth_permission` VALUES (52, 'Can view worker', 13, 'view_workerstate');
INSERT INTO `auth_permission` VALUES (53, 'Can add IDC数据中心', 14, 'add_idc');
INSERT INTO `auth_permission` VALUES (54, 'Can change IDC数据中心', 14, 'change_idc');
INSERT INTO `auth_permission` VALUES (55, 'Can delete IDC数据中心', 14, 'delete_idc');
INSERT INTO `auth_permission` VALUES (56, 'Can view IDC数据中心', 14, 'view_idc');
INSERT INTO `auth_permission` VALUES (57, 'Can add 中间件信息表', 15, 'add_middleware');
INSERT INTO `auth_permission` VALUES (58, 'Can change 中间件信息表', 15, 'change_middleware');
INSERT INTO `auth_permission` VALUES (59, 'Can delete 中间件信息表', 15, 'delete_middleware');
INSERT INTO `auth_permission` VALUES (60, 'Can view 中间件信息表', 15, 'view_middleware');
INSERT INTO `auth_permission` VALUES (61, 'Can add 备件表', 16, 'add_spareparts');
INSERT INTO `auth_permission` VALUES (62, 'Can change 备件表', 16, 'change_spareparts');
INSERT INTO `auth_permission` VALUES (63, 'Can delete 备件表', 16, 'delete_spareparts');
INSERT INTO `auth_permission` VALUES (64, 'Can view 备件表', 16, 'view_spareparts');
INSERT INTO `auth_permission` VALUES (65, 'Can add 标签表', 17, 'add_tags');
INSERT INTO `auth_permission` VALUES (66, 'Can change 标签表', 17, 'change_tags');
INSERT INTO `auth_permission` VALUES (67, 'Can delete 标签表', 17, 'delete_tags');
INSERT INTO `auth_permission` VALUES (68, 'Can view 标签表', 17, 'view_tags');
INSERT INTO `auth_permission` VALUES (69, 'Can add 备件更换表', 18, 'add_spareout');
INSERT INTO `auth_permission` VALUES (70, 'Can change 备件更换表', 18, 'change_spareout');
INSERT INTO `auth_permission` VALUES (71, 'Can delete 备件更换表', 18, 'delete_spareout');
INSERT INTO `auth_permission` VALUES (72, 'Can view 备件更换表', 18, 'view_spareout');
INSERT INTO `auth_permission` VALUES (73, 'Can add 服务器资产表', 19, 'add_serverassets');
INSERT INTO `auth_permission` VALUES (74, 'Can change 服务器资产表', 19, 'change_serverassets');
INSERT INTO `auth_permission` VALUES (75, 'Can delete 服务器资产表', 19, 'delete_serverassets');
INSERT INTO `auth_permission` VALUES (76, 'Can view 服务器资产表', 19, 'view_serverassets');
INSERT INTO `auth_permission` VALUES (77, 'Can add 网络设备', 20, 'add_netassets');
INSERT INTO `auth_permission` VALUES (78, 'Can change 网络设备', 20, 'change_netassets');
INSERT INTO `auth_permission` VALUES (79, 'Can delete 网络设备', 20, 'delete_netassets');
INSERT INTO `auth_permission` VALUES (80, 'Can view 网络设备', 20, 'view_netassets');
INSERT INTO `auth_permission` VALUES (81, 'Can add 数据字典', 21, 'add_dict');
INSERT INTO `auth_permission` VALUES (82, 'Can change 数据字典', 21, 'change_dict');
INSERT INTO `auth_permission` VALUES (83, 'Can delete 数据字典', 21, 'delete_dict');
INSERT INTO `auth_permission` VALUES (84, 'Can view 数据字典', 21, 'view_dict');
INSERT INTO `auth_permission` VALUES (85, 'Can add 菜单', 22, 'add_menu');
INSERT INTO `auth_permission` VALUES (86, 'Can change 菜单', 22, 'change_menu');
INSERT INTO `auth_permission` VALUES (87, 'Can delete 菜单', 22, 'delete_menu');
INSERT INTO `auth_permission` VALUES (88, 'Can view 菜单', 22, 'view_menu');
INSERT INTO `auth_permission` VALUES (89, 'Can add 权限', 23, 'add_permission');
INSERT INTO `auth_permission` VALUES (90, 'Can change 权限', 23, 'change_permission');
INSERT INTO `auth_permission` VALUES (91, 'Can delete 权限', 23, 'delete_permission');
INSERT INTO `auth_permission` VALUES (92, 'Can view 权限', 23, 'view_permission');
INSERT INTO `auth_permission` VALUES (93, 'Can add 角色', 24, 'add_role');
INSERT INTO `auth_permission` VALUES (94, 'Can change 角色', 24, 'change_role');
INSERT INTO `auth_permission` VALUES (95, 'Can delete 角色', 24, 'delete_role');
INSERT INTO `auth_permission` VALUES (96, 'Can view 角色', 24, 'view_role');
INSERT INTO `auth_permission` VALUES (97, 'Can add 用户信息', 25, 'add_userprofile');
INSERT INTO `auth_permission` VALUES (98, 'Can change 用户信息', 25, 'change_userprofile');
INSERT INTO `auth_permission` VALUES (99, 'Can delete 用户信息', 25, 'delete_userprofile');
INSERT INTO `auth_permission` VALUES (100, 'Can view 用户信息', 25, 'view_userprofile');

-- ----------------------------
-- Table structure for celery_taskmeta
-- ----------------------------
DROP TABLE IF EXISTS `celery_taskmeta`;
CREATE TABLE `celery_taskmeta`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `result` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `date_done` datetime(6) NOT NULL,
  `traceback` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `hidden` tinyint(1) NOT NULL,
  `meta` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `task_id`(`task_id`) USING BTREE,
  INDEX `celery_taskmeta_hidden_23fd02dc`(`hidden`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of celery_taskmeta
-- ----------------------------

-- ----------------------------
-- Table structure for celery_tasksetmeta
-- ----------------------------
DROP TABLE IF EXISTS `celery_tasksetmeta`;
CREATE TABLE `celery_tasksetmeta`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taskset_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `result` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `date_done` datetime(6) NOT NULL,
  `hidden` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `taskset_id`(`taskset_id`) USING BTREE,
  INDEX `celery_tasksetmeta_hidden_593cfc24`(`hidden`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of celery_tasksetmeta
-- ----------------------------

-- ----------------------------
-- Table structure for django_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `django_admin_log`;
CREATE TABLE `django_admin_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `object_repr` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `content_type_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `django_admin_log_content_type_id_c4bce8eb_fk_django_co`(`content_type_id`) USING BTREE,
  INDEX `django_admin_log_user_id_c564eba6_fk_rbac_userprofile_id`(`user_id`) USING BTREE,
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_rbac_userprofile_id` FOREIGN KEY (`user_id`) REFERENCES `rbac_userprofile` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 74 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of django_admin_log
-- ----------------------------
INSERT INTO `django_admin_log` VALUES (57, '2020-05-28 07:20:34.082364', '1', 'admin', 3, '', 25, 3);
INSERT INTO `django_admin_log` VALUES (58, '2020-05-28 07:20:34.093306', '2', 'test', 3, '', 25, 3);
INSERT INTO `django_admin_log` VALUES (59, '2020-05-28 07:21:00.613519', '2', 'admin', 1, '[{\"added\": {}}]', 24, 3);
INSERT INTO `django_admin_log` VALUES (60, '2020-05-28 07:21:16.376212', '1', 'admin', 2, '[{\"changed\": {\"fields\": [\"method\"]}}]', 23, 3);
INSERT INTO `django_admin_log` VALUES (61, '2020-05-28 07:21:27.641145', '3', 'admin2', 2, '[{\"changed\": {\"fields\": [\"last_login\", \"name\", \"mobile\", \"roles\"]}}]', 25, 3);
INSERT INTO `django_admin_log` VALUES (62, '2020-05-28 07:22:19.132917', '10', '菜单管理', 1, '[{\"added\": {}}]', 22, 3);
INSERT INTO `django_admin_log` VALUES (63, '2020-05-28 07:22:34.151446', '10', '菜单管理', 2, '[{\"changed\": {\"fields\": [\"icon\", \"path\"]}}]', 22, 3);
INSERT INTO `django_admin_log` VALUES (64, '2020-05-28 07:22:44.273522', '10', '菜单管理', 2, '[{\"changed\": {\"fields\": [\"is_show\"]}}]', 22, 3);
INSERT INTO `django_admin_log` VALUES (65, '2020-05-28 07:22:58.031071', '10', '菜单管理', 2, '[{\"changed\": {\"fields\": [\"icon\", \"path\"]}}]', 22, 3);
INSERT INTO `django_admin_log` VALUES (66, '2020-05-28 07:23:04.551117', '10', '菜单管理', 2, '[{\"changed\": {\"fields\": [\"is_frame\", \"is_show\"]}}]', 22, 3);
INSERT INTO `django_admin_log` VALUES (67, '2020-05-28 07:23:20.548347', '10', '菜单管理', 2, '[{\"changed\": {\"fields\": [\"is_frame\", \"component\"]}}]', 22, 3);
INSERT INTO `django_admin_log` VALUES (68, '2020-05-28 07:23:42.954551', '10', '菜单管理', 2, '[{\"changed\": {\"fields\": [\"path\"]}}]', 22, 3);
INSERT INTO `django_admin_log` VALUES (69, '2020-05-28 07:23:51.327013', '10', '菜单管理', 2, '[{\"changed\": {\"fields\": [\"icon\"]}}]', 22, 3);
INSERT INTO `django_admin_log` VALUES (70, '2020-05-28 07:23:56.991676', '10', '菜单管理', 2, '[{\"changed\": {\"fields\": [\"component\"]}}]', 22, 3);
INSERT INTO `django_admin_log` VALUES (71, '2020-05-28 07:24:10.280865', '10', '菜单管理', 2, '[{\"changed\": {\"fields\": [\"icon\", \"path\"]}}]', 22, 3);
INSERT INTO `django_admin_log` VALUES (72, '2020-05-28 07:25:02.682852', '10', '菜单管理', 3, '', 22, 3);
INSERT INTO `django_admin_log` VALUES (73, '2020-05-28 07:25:09.316130', '8', '用户管理', 2, '[{\"changed\": {\"fields\": [\"component\"]}}]', 22, 3);

-- ----------------------------
-- Table structure for django_content_type
-- ----------------------------
DROP TABLE IF EXISTS `django_content_type`;
CREATE TABLE `django_content_type`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `model` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `django_content_type_app_label_model_76bd3d3b_uniq`(`app_label`, `model`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of django_content_type
-- ----------------------------
INSERT INTO `django_content_type` VALUES (1, 'admin', 'logentry');
INSERT INTO `django_content_type` VALUES (21, 'assets', 'dict');
INSERT INTO `django_content_type` VALUES (14, 'assets', 'idc');
INSERT INTO `django_content_type` VALUES (15, 'assets', 'middleware');
INSERT INTO `django_content_type` VALUES (20, 'assets', 'netassets');
INSERT INTO `django_content_type` VALUES (19, 'assets', 'serverassets');
INSERT INTO `django_content_type` VALUES (18, 'assets', 'spareout');
INSERT INTO `django_content_type` VALUES (16, 'assets', 'spareparts');
INSERT INTO `django_content_type` VALUES (17, 'assets', 'tags');
INSERT INTO `django_content_type` VALUES (3, 'auth', 'group');
INSERT INTO `django_content_type` VALUES (2, 'auth', 'permission');
INSERT INTO `django_content_type` VALUES (4, 'contenttypes', 'contenttype');
INSERT INTO `django_content_type` VALUES (6, 'djcelery', 'crontabschedule');
INSERT INTO `django_content_type` VALUES (7, 'djcelery', 'intervalschedule');
INSERT INTO `django_content_type` VALUES (8, 'djcelery', 'periodictask');
INSERT INTO `django_content_type` VALUES (9, 'djcelery', 'periodictasks');
INSERT INTO `django_content_type` VALUES (10, 'djcelery', 'taskmeta');
INSERT INTO `django_content_type` VALUES (11, 'djcelery', 'tasksetmeta');
INSERT INTO `django_content_type` VALUES (12, 'djcelery', 'taskstate');
INSERT INTO `django_content_type` VALUES (13, 'djcelery', 'workerstate');
INSERT INTO `django_content_type` VALUES (22, 'rbac', 'menu');
INSERT INTO `django_content_type` VALUES (23, 'rbac', 'permission');
INSERT INTO `django_content_type` VALUES (24, 'rbac', 'role');
INSERT INTO `django_content_type` VALUES (25, 'rbac', 'userprofile');
INSERT INTO `django_content_type` VALUES (5, 'sessions', 'session');

-- ----------------------------
-- Table structure for django_migrations
-- ----------------------------
DROP TABLE IF EXISTS `django_migrations`;
CREATE TABLE `django_migrations`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of django_migrations
-- ----------------------------
INSERT INTO `django_migrations` VALUES (1, 'contenttypes', '0001_initial', '2020-05-28 06:05:55.521945');
INSERT INTO `django_migrations` VALUES (2, 'contenttypes', '0002_remove_content_type_name', '2020-05-28 06:05:55.646659');
INSERT INTO `django_migrations` VALUES (3, 'auth', '0001_initial', '2020-05-28 06:05:55.748350');
INSERT INTO `django_migrations` VALUES (4, 'auth', '0002_alter_permission_name_max_length', '2020-05-28 06:05:56.112405');
INSERT INTO `django_migrations` VALUES (5, 'auth', '0003_alter_user_email_max_length', '2020-05-28 06:05:56.122342');
INSERT INTO `django_migrations` VALUES (6, 'auth', '0004_alter_user_username_opts', '2020-05-28 06:05:56.130319');
INSERT INTO `django_migrations` VALUES (7, 'auth', '0005_alter_user_last_login_null', '2020-05-28 06:05:56.145305');
INSERT INTO `django_migrations` VALUES (8, 'auth', '0006_require_contenttypes_0002', '2020-05-28 06:05:56.151263');
INSERT INTO `django_migrations` VALUES (9, 'auth', '0007_alter_validators_add_error_messages', '2020-05-28 06:05:56.191157');
INSERT INTO `django_migrations` VALUES (10, 'auth', '0008_alter_user_username_max_length', '2020-05-28 06:05:56.201163');
INSERT INTO `django_migrations` VALUES (11, 'auth', '0009_alter_user_last_name_max_length', '2020-05-28 06:05:56.211103');
INSERT INTO `django_migrations` VALUES (12, 'auth', '0010_alter_group_name_max_length', '2020-05-28 06:05:56.286941');
INSERT INTO `django_migrations` VALUES (13, 'auth', '0011_update_proxy_permissions', '2020-05-28 06:05:56.294912');
INSERT INTO `django_migrations` VALUES (14, 'rbac', '0001_initial', '2020-05-28 06:05:56.611034');
INSERT INTO `django_migrations` VALUES (15, 'admin', '0001_initial', '2020-05-28 06:05:58.380304');
INSERT INTO `django_migrations` VALUES (16, 'admin', '0002_logentry_remove_auto_add', '2020-05-28 06:05:58.542903');
INSERT INTO `django_migrations` VALUES (17, 'admin', '0003_logentry_add_action_flag_choices', '2020-05-28 06:05:58.555834');
INSERT INTO `django_migrations` VALUES (18, 'assets', '0001_initial', '2020-05-28 06:05:58.956763');
INSERT INTO `django_migrations` VALUES (19, 'djcelery', '0001_initial', '2020-05-28 06:06:00.088737');
INSERT INTO `django_migrations` VALUES (20, 'sessions', '0001_initial', '2020-05-28 06:06:00.561473');

-- ----------------------------
-- Table structure for django_session
-- ----------------------------
DROP TABLE IF EXISTS `django_session`;
CREATE TABLE `django_session`  (
  `session_key` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `session_data` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`) USING BTREE,
  INDEX `django_session_expire_date_a5c62663`(`expire_date`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of django_session
-- ----------------------------
INSERT INTO `django_session` VALUES ('r83145icsk91xg31k8zgnsgu57nj4e6b', 'NzdlY2RiN2E5YTQ1NGU1NDUwNTM4N2NmYTA0OGNmNTVhZWNhZTZhYzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIzZGNhMWQ1YjNiMTE4Yjg1YTc4NzYwYWM1MzE0ZTY1OTRjMjM5ZjhkIn0=', '2020-06-11 06:06:47.115119');
INSERT INTO `django_session` VALUES ('vqskef9iisu5owrt1sv3fqkyekflz8hr', 'MGE5YTc0MGVmZmQyZmVkNDg5ZjhiNzVhYzcwNTFhMjJkZmVhODQyODp7Il9hdXRoX3VzZXJfaWQiOiIzIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJkMWM5OWY4ZjNlMGNiMmYzZTNiZmFhMjIyNzI5NTViMTQ0ZjI1NDFhIn0=', '2020-06-11 07:20:19.406160');

-- ----------------------------
-- Table structure for djcelery_crontabschedule
-- ----------------------------
DROP TABLE IF EXISTS `djcelery_crontabschedule`;
CREATE TABLE `djcelery_crontabschedule`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `minute` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `hour` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `day_of_week` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `day_of_month` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `month_of_year` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of djcelery_crontabschedule
-- ----------------------------

-- ----------------------------
-- Table structure for djcelery_intervalschedule
-- ----------------------------
DROP TABLE IF EXISTS `djcelery_intervalschedule`;
CREATE TABLE `djcelery_intervalschedule`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `every` int(11) NOT NULL,
  `period` varchar(24) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of djcelery_intervalschedule
-- ----------------------------

-- ----------------------------
-- Table structure for djcelery_periodictask
-- ----------------------------
DROP TABLE IF EXISTS `djcelery_periodictask`;
CREATE TABLE `djcelery_periodictask`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `task` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `args` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `kwargs` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `queue` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `exchange` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `routing_key` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `expires` datetime(6) NULL DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `last_run_at` datetime(6) NULL DEFAULT NULL,
  `total_run_count` int(10) UNSIGNED NOT NULL,
  `date_changed` datetime(6) NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `crontab_id` int(11) NULL DEFAULT NULL,
  `interval_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE,
  INDEX `djcelery_periodictas_crontab_id_75609bab_fk_djcelery_`(`crontab_id`) USING BTREE,
  INDEX `djcelery_periodictas_interval_id_b426ab02_fk_djcelery_`(`interval_id`) USING BTREE,
  CONSTRAINT `djcelery_periodictas_crontab_id_75609bab_fk_djcelery_` FOREIGN KEY (`crontab_id`) REFERENCES `djcelery_crontabschedule` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `djcelery_periodictas_interval_id_b426ab02_fk_djcelery_` FOREIGN KEY (`interval_id`) REFERENCES `djcelery_intervalschedule` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of djcelery_periodictask
-- ----------------------------

-- ----------------------------
-- Table structure for djcelery_periodictasks
-- ----------------------------
DROP TABLE IF EXISTS `djcelery_periodictasks`;
CREATE TABLE `djcelery_periodictasks`  (
  `ident` smallint(6) NOT NULL,
  `last_update` datetime(6) NOT NULL,
  PRIMARY KEY (`ident`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of djcelery_periodictasks
-- ----------------------------

-- ----------------------------
-- Table structure for djcelery_taskstate
-- ----------------------------
DROP TABLE IF EXISTS `djcelery_taskstate`;
CREATE TABLE `djcelery_taskstate`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `task_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tstamp` datetime(6) NOT NULL,
  `args` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `kwargs` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `eta` datetime(6) NULL DEFAULT NULL,
  `expires` datetime(6) NULL DEFAULT NULL,
  `result` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `traceback` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `runtime` double NULL DEFAULT NULL,
  `retries` int(11) NOT NULL,
  `hidden` tinyint(1) NOT NULL,
  `worker_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `task_id`(`task_id`) USING BTREE,
  INDEX `djcelery_taskstate_state_53543be4`(`state`) USING BTREE,
  INDEX `djcelery_taskstate_name_8af9eded`(`name`) USING BTREE,
  INDEX `djcelery_taskstate_tstamp_4c3f93a1`(`tstamp`) USING BTREE,
  INDEX `djcelery_taskstate_hidden_c3905e57`(`hidden`) USING BTREE,
  INDEX `djcelery_taskstate_worker_id_f7f57a05_fk_djcelery_workerstate_id`(`worker_id`) USING BTREE,
  CONSTRAINT `djcelery_taskstate_worker_id_f7f57a05_fk_djcelery_workerstate_id` FOREIGN KEY (`worker_id`) REFERENCES `djcelery_workerstate` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of djcelery_taskstate
-- ----------------------------

-- ----------------------------
-- Table structure for djcelery_workerstate
-- ----------------------------
DROP TABLE IF EXISTS `djcelery_workerstate`;
CREATE TABLE `djcelery_workerstate`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `last_heartbeat` datetime(6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `hostname`(`hostname`) USING BTREE,
  INDEX `djcelery_workerstate_last_heartbeat_4539b544`(`last_heartbeat`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of djcelery_workerstate
-- ----------------------------

-- ----------------------------
-- Table structure for rbac_menu
-- ----------------------------
DROP TABLE IF EXISTS `rbac_menu`;
CREATE TABLE `rbac_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `path` varchar(158) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_frame` tinyint(1) NOT NULL,
  `is_show` tinyint(1) NOT NULL,
  `sort` int(11) NULL DEFAULT NULL,
  `component` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pid_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE,
  INDEX `rbac_menu_pid_id_a43b3c84_fk_rbac_menu_id`(`pid_id`) USING BTREE,
  CONSTRAINT `rbac_menu_pid_id_a43b3c84_fk_rbac_menu_id` FOREIGN KEY (`pid_id`) REFERENCES `rbac_menu` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rbac_menu
-- ----------------------------
INSERT INTO `rbac_menu` VALUES (4, '系统管理', 'system', 'system', 0, 1, 4, 'system', NULL);
INSERT INTO `rbac_menu` VALUES (8, '用户管理', 'user', 'users', 0, 1, 1, 'system/user/index', 4);
INSERT INTO `rbac_menu` VALUES (11, '菜单管理', 'chart', 'menus', 0, 1, 999, 'system/menu/index', 4);
INSERT INTO `rbac_menu` VALUES (12, '权限管理', 'lock', 'permission', 0, 1, 999, 'system/permission/index', 4);
INSERT INTO `rbac_menu` VALUES (13, '角色管理', 'form', 'roles', 0, 1, 999, 'system/role/index', 4);
INSERT INTO `rbac_menu` VALUES (14, '资产管理', 'assets', 'assets', 0, 1, 1, 'assets', NULL);
INSERT INTO `rbac_menu` VALUES (15, '服务器管理', 'server', 'servers', 0, 1, 999, 'cmdb/server/index', 14);
INSERT INTO `rbac_menu` VALUES (16, 'IDC管理', 'idc', 'idcs', 0, 1, 999, 'cmdb/idc/index', 14);
INSERT INTO `rbac_menu` VALUES (17, '数据字典', 'dict', 'dicts', 0, 1, 999, 'cmdb/dict/index', 14);
INSERT INTO `rbac_menu` VALUES (18, '网络设备管理', 'router', 'networks', 0, 1, 999, 'cmdb/network/index', 14);
INSERT INTO `rbac_menu` VALUES (19, '标签管理', 'tags', 'tags', 0, 1, 999, 'cmdb/tags/index', 14);
INSERT INTO `rbac_menu` VALUES (20, '中间件管理', 'database', 'databases', 0, 1, 999, 'cmdb/weblogic/index', 14);

-- ----------------------------
-- Table structure for rbac_permission
-- ----------------------------
DROP TABLE IF EXISTS `rbac_permission`;
CREATE TABLE `rbac_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `desc` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `method` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pid_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE,
  INDEX `rbac_permission_pid_id_6939354d_fk_rbac_permission_id`(`pid_id`) USING BTREE,
  CONSTRAINT `rbac_permission_pid_id_6939354d_fk_rbac_permission_id` FOREIGN KEY (`pid_id`) REFERENCES `rbac_permission` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rbac_permission
-- ----------------------------
INSERT INTO `rbac_permission` VALUES (1, 'admin', 'admin', 'admin', 1);

-- ----------------------------
-- Table structure for rbac_role
-- ----------------------------
DROP TABLE IF EXISTS `rbac_role`;
CREATE TABLE `rbac_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `desc` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rbac_role
-- ----------------------------
INSERT INTO `rbac_role` VALUES (1, '角色1', NULL);
INSERT INTO `rbac_role` VALUES (2, 'admin', NULL);

-- ----------------------------
-- Table structure for rbac_role_menus
-- ----------------------------
DROP TABLE IF EXISTS `rbac_role_menus`;
CREATE TABLE `rbac_role_menus`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `rbac_role_menus_role_id_menu_id_579f5861_uniq`(`role_id`, `menu_id`) USING BTREE,
  INDEX `rbac_role_menus_menu_id_180f4f9a_fk_rbac_menu_id`(`menu_id`) USING BTREE,
  CONSTRAINT `rbac_role_menus_menu_id_180f4f9a_fk_rbac_menu_id` FOREIGN KEY (`menu_id`) REFERENCES `rbac_menu` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `rbac_role_menus_role_id_323259a4_fk_rbac_role_id` FOREIGN KEY (`role_id`) REFERENCES `rbac_role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rbac_role_menus
-- ----------------------------
INSERT INTO `rbac_role_menus` VALUES (2, 1, 4);
INSERT INTO `rbac_role_menus` VALUES (1, 1, 8);

-- ----------------------------
-- Table structure for rbac_role_permissions
-- ----------------------------
DROP TABLE IF EXISTS `rbac_role_permissions`;
CREATE TABLE `rbac_role_permissions`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `rbac_role_permissions_role_id_permission_id_d01303da_uniq`(`role_id`, `permission_id`) USING BTREE,
  INDEX `rbac_role_permission_permission_id_f5e1e866_fk_rbac_perm`(`permission_id`) USING BTREE,
  CONSTRAINT `rbac_role_permission_permission_id_f5e1e866_fk_rbac_perm` FOREIGN KEY (`permission_id`) REFERENCES `rbac_permission` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `rbac_role_permissions_role_id_d10416cb_fk_rbac_role_id` FOREIGN KEY (`role_id`) REFERENCES `rbac_role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rbac_role_permissions
-- ----------------------------
INSERT INTO `rbac_role_permissions` VALUES (1, 1, 1);
INSERT INTO `rbac_role_permissions` VALUES (2, 2, 1);

-- ----------------------------
-- Table structure for rbac_userprofile
-- ----------------------------
DROP TABLE IF EXISTS `rbac_userprofile`;
CREATE TABLE `rbac_userprofile`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `last_login` datetime(6) NULL DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `first_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `last_name` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `mobile` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `image` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `position` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `superior_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE,
  INDEX `rbac_userprofile_superior_id_5d0a3780_fk_rbac_userprofile_id`(`superior_id`) USING BTREE,
  CONSTRAINT `rbac_userprofile_superior_id_5d0a3780_fk_rbac_userprofile_id` FOREIGN KEY (`superior_id`) REFERENCES `rbac_userprofile` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rbac_userprofile
-- ----------------------------
INSERT INTO `rbac_userprofile` VALUES (3, 'pbkdf2_sha256$150000$gxTITePQ9ltG$MdLXB3U2Q2WpoueikIMZbRsHjFDAk3FK+QJYmuJHfyw=', '2020-05-28 07:20:00.000000', 1, 'admin2', '', '', 1, 1, '2020-05-28 07:20:00.000000', 'asdas', '1868468023', 'admin@qq.com', 'media/image/default.gif', NULL, NULL);

-- ----------------------------
-- Table structure for rbac_userprofile_groups
-- ----------------------------
DROP TABLE IF EXISTS `rbac_userprofile_groups`;
CREATE TABLE `rbac_userprofile_groups`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userprofile_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `rbac_userprofile_groups_userprofile_id_group_id_4d710b30_uniq`(`userprofile_id`, `group_id`) USING BTREE,
  INDEX `rbac_userprofile_groups_group_id_2c47610b_fk_auth_group_id`(`group_id`) USING BTREE,
  CONSTRAINT `rbac_userprofile_gro_userprofile_id_9cacde37_fk_rbac_user` FOREIGN KEY (`userprofile_id`) REFERENCES `rbac_userprofile` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `rbac_userprofile_groups_group_id_2c47610b_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rbac_userprofile_groups
-- ----------------------------

-- ----------------------------
-- Table structure for rbac_userprofile_roles
-- ----------------------------
DROP TABLE IF EXISTS `rbac_userprofile_roles`;
CREATE TABLE `rbac_userprofile_roles`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userprofile_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `rbac_userprofile_roles_userprofile_id_role_id_ba9254c5_uniq`(`userprofile_id`, `role_id`) USING BTREE,
  INDEX `rbac_userprofile_roles_role_id_ddc12d7e_fk_rbac_role_id`(`role_id`) USING BTREE,
  CONSTRAINT `rbac_userprofile_rol_userprofile_id_3a7afbe9_fk_rbac_user` FOREIGN KEY (`userprofile_id`) REFERENCES `rbac_userprofile` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `rbac_userprofile_roles_role_id_ddc12d7e_fk_rbac_role_id` FOREIGN KEY (`role_id`) REFERENCES `rbac_role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rbac_userprofile_roles
-- ----------------------------
INSERT INTO `rbac_userprofile_roles` VALUES (2, 3, 2);

-- ----------------------------
-- Table structure for rbac_userprofile_user_permissions
-- ----------------------------
DROP TABLE IF EXISTS `rbac_userprofile_user_permissions`;
CREATE TABLE `rbac_userprofile_user_permissions`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userprofile_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `rbac_userprofile_user_pe_userprofile_id_permissio_16bbd20f_uniq`(`userprofile_id`, `permission_id`) USING BTREE,
  INDEX `rbac_userprofile_use_permission_id_740ad67c_fk_auth_perm`(`permission_id`) USING BTREE,
  CONSTRAINT `rbac_userprofile_use_permission_id_740ad67c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `rbac_userprofile_use_userprofile_id_38afd71d_fk_rbac_user` FOREIGN KEY (`userprofile_id`) REFERENCES `rbac_userprofile` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rbac_userprofile_user_permissions
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
