# @Author     ：SEED
# @DateTime   ：2019-08-27  11:12 
# @FileName   ：gunicorn-config.py

from multiprocessing import cpu_count
bind = ["127.0.0.1:8000"]
workers = cpu_count() * 2
timeout = 60
graceful_timeout = 10
loglevel = "error"
# 错误日志
errorlog = "/var/log/ops2/gunicorn_error.log"
keepalive = 6
