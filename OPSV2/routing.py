# @Author     ：SEED
# @DateTime   ：2019-09-02  09:22 
# @FileName   ：routing.py

from channels.auth import AuthMiddlewareStack
from websocket.jwt_auth import TokenAuthMiddleware
from channels.routing import ProtocolTypeRouter, URLRouter
from django.urls import path
from apps.websocket.consumers.webssh import WebSSH
from apps.websocket.consumers.message import ConsoleMsgConsumer
from apps.websocket.consumers.guacamole import WebGuacamole

application = ProtocolTypeRouter({
    'websocket': TokenAuthMiddleware(
        URLRouter([
            path("websocket/console", WebSSH),
            path("websocket/message", ConsoleMsgConsumer),
            path("websocket/guacamole", WebGuacamole)
        ])
    ),
})
