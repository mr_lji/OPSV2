# @Author     ：SEED
# @DateTime   ：2019-08-26  09:56 
# @FileName   ：celery.py
import os
import django
from celery import Celery
from django.conf import settings

# 设置环境变量
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'OPSV2.settings')
# 实例化Celery
app = Celery('OPSV2')
# Celery加载所有注册的应用
app.config_from_object('django.conf:settings')
# 使用django的settings文件配置celery
app.autodiscover_tasks()

#
# @app.task(bind=True)
# def debug_task(self):
#     print('Request: {0!r}'.format(self.request))
