# @Author     ：SEED
# @DateTime   ：2019-09-04  16:35 
# @FileName   ：asgi.py.py

import os
import django
from channels.routing import get_default_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "OPSV2.settings")
django.setup()
application = get_default_application()